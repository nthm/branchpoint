import Koa from 'koa';

import https from 'https';
import http from 'http';
import tls from 'tls';
import ip from 'internal-ip';
import debug from 'debug';
import session from 'koa-session';

import { config, setupConfig } from './config';
// Setup and parse configuration before modules will use it
setupConfig();

import { cors } from './use/headers/cors';
import { csp } from './use/headers/csp';

const log = debug('server');

const app = new Koa();

const sessionParser = session({
  key: 'koa:sess',
  renew: true,
  signed: true,
  // TODO: Use encode/decode
}, app);

// Error handler
app.use(async (ctx, next) => {
  try {
    await next();
  } catch (error) {
    const { statusCode, status } = error;
    // TODO: Alternative to HttpStatus?
    ctx.status = statusCode || status || 500;
    error.status = ctx.status;
    // TODO: Depend on what the client accepts and use an SSR'd error page
    ctx.body = { error };
    // TODO: RESEARCH: Emitting to? `app.on('error', (err, ctx?) => {})` metric
    ctx.app.emit('error', error, ctx);
  }
});

// Logger
app.use(async (ctx, next) => {
  await next();
  const rt = ctx.response.get('X-Response-Time');
  console.log(`${ctx.method} ${ctx.url} - ${rt}`);
});

// Set X-Response-Time
app.use(async (ctx, next) => {
  const start = Date.now();
  await next();
  const ms = Date.now() - start;
  ctx.set('X-Response-Time', `${ms}ms`);
});

// Aside from headers, metrics, authentication, and websocket upgrades, another
// idea is to do errorHandling via Youch here. Basically handling proxy errors?

// Could be an explicit opt-in via a HTTP header? Stack trace is lost unless
// serialized over HTTP though

app.use(sessionParser);
app.use(cors);
app.use(csp);

// TODO: Not very interested in storing domains outside of config, absolutely
// not in the main index.ts. Maybe in ./subdomains/[domain].ts
const secureContexts: { [domain: string]: tls.SecureContext } = {
  'sub.domain.xyz': tls.createSecureContext({}),
};

(async () => {
  const host = config.LISTEN_ADDRESS === 'localhost'
    ? config.LISTEN_ADDRESS
    : await ip.v4();

  const httpServer = http.createServer(
    config.REDIRECT_TO_HTTPS
      ? (req, res) => {
        res.writeHead(301, {
          Location: `https://${req.headers.host}${req.url}`,
        });
        res.end();
      }
      : app.callback());
  httpServer.listen({
    host: config.LISTEN_ADDRESS,
    port: config.HTTP_PORT,
  }, () => {
    const serverType = config.REDIRECT_TO_HTTPS
      ? 'HTTPS redirection server'
      : 'Server';
    log(`${serverType} listening on http://${host}:${config.HTTP_PORT}`);
  });

  const httpsServer = https.createServer({
    SNICallback(serverName, callback) {
      if (serverName in secureContexts) {
        // tslint:disable-next-line: no-null-keyword
        return callback(null, secureContexts[serverName]);
      }
      log(`Unknown domain for SNI request: '${serverName}'`);
    },
  }, app.callback());
  httpsServer.listen({
    host: config.LISTEN_ADDRESS,
    port: config.HTTPS_PORT,
  }, () => {
    log(`Server listening on https://${host}:${config.HTTPS_PORT}`);
  });

  // TODO: This is only a proxy server - how does that change anything?
  // setupWebsocketHandler(httpsServer, sessionParser);
})();
