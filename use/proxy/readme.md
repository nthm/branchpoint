Proxy

This handles connections between clients and services other than Branchpoint
itself. It's a generic proxy that aimed to update [koa-better-http-proxy][1]
and reduce its size and dependencies.

[1] https://github.com/nsimmons/koa-better-http-proxy
