import Koa from 'koa';

// Handle CSP
// https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP

// Not interested in X-XSS-Protection, X-Content-Type-Options, Referrer-Policy,
// X-Frame-Options, and Strict-Transport-Security (HSTS) for now

const cspString = "default-src 'self' localhost:* wss://localhost:*; object-src 'none'";
export const csp: Koa.Middleware = (ctx, next) => {
  ctx.set('Content-Security-Policy', cspString);
  return next();

  // TODO:
  // - Consider supporting a reporting endpoint for CSP violations
  // - Generate nonces and hashes of scripts/styles on the fly for SSR
};
