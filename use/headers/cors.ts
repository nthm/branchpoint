import Koa from 'koa';

// Handles CORS
// https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS

export const cors: Koa.Middleware = (ctx, next) => {
  // CORS packages in Express and Koa is long, complicated, and over engineered
  // This is fine; just be mindful of the `Vary:` header

  // Some CORS middlware _respond_ with *-Request-{Method,Headers}, often
  // reflecting their values, but MDN doesn't say to do that

  ctx.set({
    // Note browsers may not want to leak credentials if Allow-Origin is '*'
    'Access-Control-Allow-Origin': '*',
    // If preflight, allows attaching credentials; else, exposes the response
    'Access-Control-Allow-Credentials': 'true',
    // Let clients access these in userland:
    'Access-Control-Expose-Headers': 'X-Response-Time',
  });

  // Always set the `Vary: Origin`: https://github.com/rs/cors/issues/10
  ctx.vary('Origin');

  if (ctx.method !== 'OPTIONS') {
    // Actual request. Just: Origin, Credentials, Expose-Headers
    return next();
  }

  // Preflight requests. Both:
  // Origin, Credentials, Expose-Headers, and then also Methods, Max-Age
  ctx.set({
    'Access-Control-Allow-Methods': 'GET,HEAD,PUT,PATCH,POST,DELETE',
    'Access-Control-Max-Age': '3600', // 1 hour cache
  });

  // Safari (and potentially other browsers) need content-length 0, for 204 or
  // they hang waiting for a body
  ctx.status = 204;
  ctx.set('Content-Length', '0');

  // Respond
  return;
};
