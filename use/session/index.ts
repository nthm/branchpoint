// koa-session will emit event on app when session expired or invalid:

//     session:missed: can't get session value from external store.
//     session:invalid: session value is invalid.
//     session:expired: session value is expired.

// I'll probably write my own session store lol

// P.S I want to write my own metrics, obviously, but I think using Netdata
// and/or Grafana is good. Netdata for real time.
