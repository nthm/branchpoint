This is a proxy server with a primary focus on SSL termination. Virtual hosts
are new processes started elsewhere, likely by a systemd service. There is no
frontend or client. It exposes at most an API.

Middleware like compression, metrics, and authentication should be per host and
developed in a separate projects where possible. This becomes harder for
integrations like _koa-session_. All endpoints provided by middlware should be
an API. Frontends will be client-only. SSR is down the road but will still use
API resolution for data. The only exception is Youch, which returns either JSON
or HTML depending on content negotiation with the client via `ctx.accepts()`.

Endpoints aren't trusted since they have large unknown dependency trees and
should be isolated in systemd-nspawn or Docker. This prevents them from talking
directly to other exposed HTTP ports on the same network interface.
