## Current:

- _koa_: Server.
- _koa-compress_: GZip. Uses _compressible_ to read mime-type and call zlib.
- _koa-session_: Lightweight concept of a session. Work with any external store.
- _koa-static_: Serve from the filesystem. TODO: Consider _lru-memoize_ package.
- _sql-template-strings_: SQL.
- _sqlite_: SQL.
- _youch_: Pretty server error logging.

## Upcoming:

Logging:

- klaussinani/signale. Requires some setup

- pablosichert/concurrency-logger. Graphs live requests and response times.
- workable/riviere. Save all requests and responses as a session story.

Security:

This would only want to be implemented once metrics are running well.

- tunnckoCore/koa-better-ratelimit which uses tunnckoCore/koa-ip-filter. Each is
  one file and well written. _Memory-only no database_.
- ~~koa/ratelimit~~ No, uses tj/ratelimiter which depends on Redis :x:

Subdomains:

- koa-subdomain
- koa-virtual-host < 80 LoC
