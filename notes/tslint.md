There's no `false` values in the config.

To be less annoying, `no-trailing-whitespace` is off and should be handled by
the editor and/or git on save and on commit hooks, respectively. This could also
be true for encoding and line endings.
