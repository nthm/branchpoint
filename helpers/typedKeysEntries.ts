export const typedKeys = Object.keys as <T>(o: T) => (Extract<keyof T, string>)[];
export const typedEntries = Object.entries as <T, K extends keyof T>(o: T) => [Extract<keyof T, string>, T[K]][];
