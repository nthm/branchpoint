## Proxy

Researched [koa-better-http-proxy][1] It's a well written project. Broken up
nicely into many files but still has a logical flow. Good documentation.
Supports all usecases I can think of except websockets.

The code disolves into about 400 lines. 200 of which is the actual proxy, and
the rest are helpers.

**However: There are many deprecation warnings from Node** and the project has
not been updated in **2 years**. It even imports a Promise polyfill to show its
age. It will need to be updated.

Proxies set a lot of headers, and although this benchmark is 3 years old, it's
apparently 3x faster to write lowercase headers:

https://gist.github.com/fengmk2/1634043ec1d40e09b53197b817411131

## Metrics

Also support passing metric IDs back and forth for proxied applications. They
should be able to extend metrics on a request. Especially since Koa will be
marking the request time.

It's not wrong to have duplicate stats, as some services like NextCloud won't
want to speak with the metrics service. Some will want to enhance the metrics
with data that Branchpoint can't see.

Request or response sizes. Status codes. Time taken. Along with the usual page
views and IP logging.

I dissolved stacktrace.js and took notes on the USB. Will post.

[1]: https://github.com/nsimmons/koa-better-http-proxy

Metrics as an external service? Yes Branchpoint will be the hub of tracking, ok,
but endpoints will want to be able to write events somewhere. Don't have two
metric locations, just tell Branchpoint to write to the one service.

For metric fields, consider:

- https://github.com/keen/keen-tracking.js
- https://help.piwik.pro/support/analytics/database-schema/

Use VIEW in SQL to have fields that rely on other data, such as `last_visited`.
Consider limiting the number of records in the database, or archiving the data
by dumping the table to a file. This can be automated by TRIGGER, and some
optimizations of that are discussed [here][2], but it's probably OK to use a
monthly systemd timer as well.

[2]: https://stackoverflow.com/questions/18677649/limiting-records-in-sqlite

Metric event for equivalent SSR'd page load time. How much time is SSR saving
people? Need to diff that with the added response time (if any? depends on SSR
policy - general cache vs per user full stateful render).

I haven't fully decided on a database, since going with SQLite might have issues
with tracking ratelimiting or IP addresses... The system just might not be fast
enough to do an SQL transaction each time.

I'll probably start with SQLite because my imagined traffic is small. The
alternative is to do a split between SQLite and InfluxDB. Obviously SQLite for
persistant data, like accounts and passwords... The lines start to blur when you
want to track the metric events of everything including sign ins and password
changes. Tricky.

## Components

There are a few points to Branchpoint, but it should be split up to have each
section be developed or used individually. This includes:

- The Koa server that handles authentication, routing, security, 404 fallback
  handlers, and most importantly, logging/metrics via sessions (note this uses
  the metrics service, described above)

- Proxy handler middleware which takes requests from the above Koa server and
  performs delegation. Must support websockets.

- Frontend for configurations and administration. This isn't necessary the index
  site that is loaded on the default route - which could be proxied to another
  server or just be a mounted static site - instead, this is the gateway page to
  see all the containers/endpoints on the server. Authentication plays a part
  into this. Basic metrics should be public.

  Need to be able to turn off a container/endpoint. Involves endpoint
  registration. Page visits. Rate. Uptime. Session hours? (Similar to Steam)

- Dynamic server blocks. Remove or add without restarting the server. Some
  blocks will be a simple proxy pass, which is fine but outside of the control
  and scope of Branchpoint. They will need to be modified directly or have their
  processes restarted. Other server blocks could be far more complicated and
  involve modifying headers or the body. They could be entire Node applications
  in Express or Koa

  > _I think. Might not want to have them share memory? How to provide isolation
  without processes and ports?_ Consider "\[project]-dev.kudasai.xyz" which will
  throw exceptions and are dangerous for RCE...

  Involves having file-seperated core server code and server blocks which are
  loaded in with `require()`. Then use:

  ```js
  // Load it into Koa, registering it somehow:
  const registerations = {
    endpointA: require('./path/to/endpoint'),
  }

  // Later, refresh it dynamically:
  // Could be `HTTP GET /refresh/<endpoint>`
  delete require.cache[require.resolve('./path/to/endpoint')]
  registerations.endpointA = require('./path/to/endpoint')
  ```

  I don't want an editor in the browser, but would be nice to iterate quickly on
  one endpoint for debugging/setup in-browser.

  I mean, use code.kudasai to make changes to a server block and then press
  reload on branchpoint.kudasai.xyz (name pending). Ideal.

## Sever side rendering

Note this only applies to my projects not anything being proxied that's a third
party like NextCloud.

So there's templates. Pug/Jade, EJS, Swig, etc. Won't help with SSR since the UI
is written in Voko. The isolated-core has support for rebinding all event
listeners and reinitializing state from an SSR'd document.

React/Preact are often served as an HTML document, maybe generated by Webpack,
which include a script tag to load the JS bundle that mounts onto the root HTML
element (maybe body or #root etc). For SSR, like in this [Preact example][1],
the `render()` method is replaced by `preact-render-to-string` which converts
the virtual-DOM tree into an HTML string.

That's the most efficient, since you don't need to emulate a browser. In Voko,
it doesn't work since it only knows how to write using DOM APIs. Need to use
JSDOM, or similar, to support a DOM. There's [koa-ssr][2] which does exactly
that to serialize the DOM into a cacheable and servable HTML file.

> Sidenote that koa-ssr is a bit old, and loads _cheerio_ to parse any given
> HTML and modify async/defer script tags which [aren't supported by JSDOM][3].
> It will go unused.

I can use koa-ssr for now, but will be better off using JSDOM more directly in
the future. After the script tag for voko.min.js loads, modify it to capture all
event listeners. Or, wrap document.addEventListener instead. Those elements will
need more hydration client side.

[1] https://github.com/lukeed/preact-cli-ssr/blob/master/server.js
[2] https://github.com/laggingreflex/koa-ssr/
[3] https://github.com/jsdom/jsdom#asynchronous-script-loading
