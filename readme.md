# Branchpoint

Interactive reverse proxy dashboard in Koa. Live reload subdomain configurations
and centralize their logging, authentication, and metrics.

Node alternative to NGINX/Apache/Caddy. Branchpoint leverages Koa, an
abstraction of Node's HTTP modules, to provide a minimal but complete webserver
that allows requests to be passed through middleware.

Having a language to work with brings many advantages over traditional reverse
proxy servers. It's transparent; there's no magic and you can run it in a
debugger. Proxies often want to perform operations like rewriting headers, load
balancing, A-B testing, metrics, or authentication. When using an application,
that means reading several pages of documentation for yet another configuration
file format; or realizing your use case isn't supported. Instead, implementing a
proxy in a language means it's only a few lines of code.

Currently this is mostly vaporware. It's follows an earlier project, a zero
dependency [Node proxy](https://gitlab.com/nthm/proxy). Using vanilla Node over
other reverse proxies works well but is held back by lack of community and
difficult API style (callback and event-emitter based) in its standard modules.
Koa has a more intuitive API and taps into a large ecosystem which allows for
more complex application than being only a reverse proxy. Branchpoint is that.
